#ifndef STATICS_H
#define STATICS_H 1

typedef enum g_mode_t{
        G_MODE_RAD,
        G_MODE_DEG,
        G_MODE_RAW
} g_mode_t;
typedef enum a_mode_t{
        A_MODE_MS2,
        A_MODE_G,
        A_MODE_RAW
} a_mode_t;


static int enable_magnetometer = 1;
static int enable_thermometer = 1;
static int enable_warnings = 0;
static int running = 0;

#endif
