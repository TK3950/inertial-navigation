#ifndef CONFIG_H
#define CONFIG_H 1

//#define DEBUG_POS 1
#define DEBUG_VEL 1
//#define DEBUG_ACC 1
//#define DEBUG_TIME 1
#define FRAME_SPACING 1
#define CALIB_SAMPLES 400
#define RAW_DEADZONE 10
#define VEL_DEADZONE 50
//#define POS_DEADZONE
// bus for Robotics Cape and BeagleboneBlue is 2
// change this for your platform
#define I2C_BUS 2
#endif
