#include <iostream>
#include <fstream>
#include <cstring>
#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <rc/mpu.h>
#include <rc/time.h>
#include "config.h"
#include "statics.h"

static void __signal_handler(__attribute__ ((unused)) int dummy) // probably useless
{
        running=0;
        return;
}

double max[3];
double min[3];
double mean[3];

void calibrate_noise_filter(); // run on startup

void getVelocity(rc_mpu_data_t, clock_t, clock_t, double[3]); // integrate mpu_data_t acceleration data into a velocity

void getTotalDisplacement(double[3], clock_t, clock_t, double[3]); // integrate double[3] velocity into displacement

int main(int argc, char *argv[])
{

	printf("begin\n");

	rc_mpu_data_t data;

	// read accel data
	rc_mag_data_t mag;

        g_mode_t g_mode = G_MODE_DEG; // gyro default to degree mode.
        a_mode_t a_mode = A_MODE_MS2; // accel default to m/s^2
        // no argument parsing

        // use defaults for now, except also enable magnetometer.
        rc_mpu_config_t conf = rc_mpu_default_config();
        conf.i2c_bus = I2C_BUS;
        conf.enable_magnetometer = 1;
        conf.show_warnings = enable_warnings;


        if(rc_mpu_initialize(&data, conf)){
                fprintf(stderr,"rc_mpu_initialize_failed\n");
                return -1;
        }




		 //infinite loop
	bool first_run = true;
	bool settling_period = true;
	int settling_count = 0;
	clock_t t0,t1;

	rc_mpu_data_t data_old;


	double velocity[3] = {0.0f,0.0f,0.0f};
	double th_vel[3];
	double displacement[3] = {0.0f,0.0f,0.0f};

	rc_mpu_read_accel(&data);
	rc_mpu_read_mag(&mag);

	calibrate_noise_filter();





        while(true){
		if (first_run) // better way?
		{
			t0 = clock();
			first_run = false;
			rc_mpu_read_mag(&mag);
		}
		else
		{
			t0=t1;
			t1=clock();
			data_old = data;
		}
                if(rc_mpu_read_accel(&data)<0){
                        printf("read accel data failed\n");
                }
                if(rc_mpu_read_gyro(&data)<0){
                        printf("read gyro data failed\n");
		}
		if(rc_mpu_read_mag(&mag)<0){
			printf("read mag data failed\n");
		}


		if (!settling_period)
		{

			// do the math
			getVelocity(data, t0, t1, velocity);
			getTotalDisplacement(velocity, t0, t1, displacement);

#ifdef DEBUG_POS
			printf("xyz: %f %f %f",
				displacement[0],
				displacement[1],
				displacement[2]);
#endif
#ifdef DEBUG_VEL
			printf("\tvel xyz: %f %f %f",
				velocity[0],
				velocity[1],
				velocity[2]);
#endif
#ifdef DEBUG_ACC
			printf("\tacc xyz: %f %f %f",
				data.accel[0],
				data.accel[1],
				data.accel[2]);
#endif

#ifdef DEBUG_TIME
			printf("\tframe: %d clocks", (int)(t1-t0));
#endif
			printf("\n");
		}
		else
		{
			settling_count++;
			if (settling_count == 50)
			{
				settling_period = false;
			}
		}

                fflush(stdout);

#ifdef FRAME_SPACING
                rc_usleep(10000); // space frames out
#endif
        }
        printf("\n");
        rc_mpu_power_off();
        return 0;
}

void getVelocity(rc_mpu_data_t sample, clock_t t0, clock_t t1, double velocity[3], double dth_mag)
{
	double dt = (double)(t1-t0)/CLOCKS_PER_SEC;


			velocity[0] += sample.accel[0]*cos(dth_mag)*dt + sample.accel[1]*sin(dth_mag)*dt; // x
			velocity[1] +=  sample.accel[1]*cos(dth_mag)*dt + sample.accel[0]*sin(dth_mag)*dt; // y
			velocity[2] += (sample.accel[2]) * dt; // velocity is raw_accel * time frame
			// not yet sure how to account for roll that changes Z accel

}

void getTotalDisplacement(double velocity[3], clock_t t0, clock_t t1, double displacement[3])
{
	double dt = (double)(t1-t0)/CLOCKS_PER_SEC;
	for (int i = 0; i < 3; i++) // for each axis (x,y,z)
	{
		displacement[i] += 0.5f * (velocity[i] *(dt));
	}
}



void calibrate_noise_filter()
{
	rc_mpu_data_t data;
        g_mode_t g_mode = G_MODE_DEG; // gyro default to degree mode.
        a_mode_t a_mode = A_MODE_MS2; // accel default to m/s^2
        rc_mpu_config_t conf = rc_mpu_default_config();
        conf.i2c_bus = I2C_BUS;
        conf.enable_magnetometer = enable_magnetometer;
        conf.show_warnings = enable_warnings;


        if(rc_mpu_initialize(&data, conf)){
                fprintf(stderr,"rc_mpu_initialize_failed\n");
        }

	printf("calibrating\n");
        rc_usleep(1000);
	printf("still calibrating\n");

	double sum[3] = {0.0f,0.0f,0.0f};

	for (int count = 0; count < CALIB_SAMPLES; count++)
	{
		if (rc_mpu_read_accel(&data)==0)
		{

			if (count == CALIB_SAMPLES/2)
				printf("halfway there\n");
			min[0] = data.accel[0];
			min[1] = data.accel[1];
			min[2] = data.accel[2];
			for (int i = 0; i<3; i++)
			{
				sum[i] += data.accel[i];
				if ( data.accel[i] > max[i])
					max[i] = data.accel[i];
				if ( data.accel[i] < min[i])
					min[i] = data.accel[i];
		                rc_usleep(100);
			}
		}
	}
	for (int i = 0; i<3; i++)
	{
		mean[i]=sum[i]/(double)CALIB_SAMPLES;
	}
	//mean[2] = -9.85f; // clamping this because calibrating doesn't seem to help
}
